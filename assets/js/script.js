$(function() {

  	$('.widget-calendar-carousel').slick({
  		
		dots: false,
		infinite: false,
		speed: 0,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint: 1199,
		  settings: {
		    slidesToShow: 5,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 992,
		  settings: {
		    slidesToShow: 4,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
		    slidesToShow: 3,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 520,
		  settings: {
		    slidesToShow: 2,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 420,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		]
	});
  	$(".main-navigation").sticky({topSpacing:0});


		var cal = $( '#calendar' ).calendario( {
			weeks : [ 'Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota' ],
			weekabbrs : [ 'Nd', 'Po', 'Wt', 'Śr', 'Cz', 'Pi', 'So' ],
			months : [ 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień' ],
			monthabbrs : [ 'Sty', 'Lu', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru' ],
			onDayClick : function( $el, $contentEl, dateProperties ) {

				for( var key in dateProperties ) {
					console.log( key + ' = ' + dateProperties[ key ] );
				}

			},
			/*caldata : codropsEvents*/
		} ),
		$month = $( '#custom-month' ).html( cal.getMonthName() ),
		$year = $( '#custom-year' ).html( cal.getYear() );

	$( '#custom-next' ).on( 'click', function() {
		cal.gotoNextMonth( updateMonthYear );
	} );
	$( '#custom-prev' ).on( 'click', function() {
		cal.gotoPreviousMonth( updateMonthYear );
	} );
	$( '#custom-current' ).on( 'click', function() {
		cal.gotoNow( updateMonthYear );
	} );

	function updateMonthYear() {				
		$month.html( cal.getMonthName() );
		$year.html( cal.getYear() );
	}
});




